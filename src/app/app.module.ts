import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FiMenuComponent } from './fi-menu/fi-menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PdfViewComponent } from './pdf-view/pdf-view.component';
import {PdfViewerModule} from "ng2-pdf-viewer";
import { ImgsComponent } from './imgs/imgs.component';
import {GalleryModule} from "ng-gallery";

@NgModule({
  declarations: [
    AppComponent,
    FiMenuComponent,
    PdfViewComponent,
    ImgsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    PdfViewerModule,
    GalleryModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
