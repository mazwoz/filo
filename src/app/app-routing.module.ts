import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {MatMenuModule} from '@angular/material/menu';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule,
    MatMenuModule]
})
export class AppRoutingModule { }
