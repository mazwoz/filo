import { Component, OnInit, Inject } from '@angular/core';
import {WatcherService} from "../watcher.service";


@Component({
  selector: 'app-fi-menu',
  templateUrl: './fi-menu.component.html',
  styleUrls: ['./fi-menu.component.css']
})
export class FiMenuComponent implements OnInit {
  shPDF : boolean = false;
  shImg : boolean = false;
  constructor(private watcherService : WatcherService) {}

  ngOnInit(): void {
    this.watcherService.togglePDF.subscribe(status=>this.shPDF = status);
    this.watcherService.toggleImg.subscribe(status=>this.shImg = status);
  }

  ShowCC() {
    this.watcherService.emitPDFChangeEvent(false);
    this.watcherService.emitImgChangeEvent(false);

  }

  ShowPDF() {
    this.watcherService.emitPDFChangeEvent(true);
    this.watcherService.emitImgChangeEvent(false);
  }

  ShowImg() {
    window.location.href="https://photos.1stmob.com/explore/recent"
    //this.watcherService.emitPDFChangeEvent(false);
    //this.watcherService.emitImgChangeEvent(true);
  }

  goHome() {
    window.location.href = "https://1stmob.com/newsite/dist/filo/";
  }

}
