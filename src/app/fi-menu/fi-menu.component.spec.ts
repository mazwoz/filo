import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiMenuComponent } from './fi-menu.component';

describe('FiMenuComponent', () => {
  let component: FiMenuComponent;
  let fixture: ComponentFixture<FiMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
