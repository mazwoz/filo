import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-pdf-view',
  template: `
  <pdf-viewer [src]="pdfSrc"
              [render-text]="true"
              style="display: block;"
  ></pdf-viewer>
  `,
  styleUrls: ['./pdf-view.component.css']
})
export class PdfViewComponent implements OnInit {

  constructor() { }
  pdfSrc = "assets/1st_Mob_History.pdf";

  ngOnInit(): void {
  }

}
