import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfViewComponent } from './pdf-view.component';
import {PdfViewerModule} from "ng2-pdf-viewer";
import {BrowserModule} from "@angular/platform-browser";
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {NgModule} from "@angular/core";

@NgModule({
  imports: [PdfViewerModule, BrowserModule],
  declarations: [PdfViewComponent],
  bootstrap: [PdfViewComponent]
})

class PdfViewModule {}

platformBrowserDynamic().bootstrapModule(PdfViewModule)
