import { Component, OnInit } from '@angular/core';
import { GalleryItem, ImageItem } from 'ng-gallery';


@Component({
  template: `
    <gallery [items]="images"></gallery>
  `
})
export class ImgsComponent implements OnInit {

  images: GalleryItem[];
  constructor() { }

  ngOnInit(): void {
    
  }

}
