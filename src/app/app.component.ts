import {Component, OnInit} from '@angular/core';
import {WatcherService} from "./watcher.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'FiLO';
  year: number = new Date().getFullYear();

  constructor(private watcherService : WatcherService) {}

  shPDF : boolean = false;
  shImg : boolean = false;

  ngOnInit() {
    this.watcherService.togglePDF.subscribe(status=>this.togglePDF(status));
    this.watcherService.toggleImg.subscribe(status=>this.toggleImg(status));
  }

  togglePDF(status : boolean) {
    this.shPDF = status;
    console.log("PDF Showing")
  }

  toggleImg(status : boolean) {
    this.shImg = status;
  }

}
