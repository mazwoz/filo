import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WatcherService {

  constructor() { }


  togglePDF : EventEmitter<boolean> = new EventEmitter<boolean>();
  toggleImg : EventEmitter<boolean> = new EventEmitter<boolean>();

  emitPDFChangeEvent(boolean) {
    this.togglePDF.emit(boolean);
  }
  getPDFChangeEmitter() {
    return this.togglePDF;
  }

  emitImgChangeEvent(boolean) {
    this.toggleImg.emit(boolean);
  }
  getImgChangeEmitter() {
    return this.toggleImg;
  }
}
